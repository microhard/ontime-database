#!/bin/bash

# Si no es tenen privilegis root
if [ $EUID -ne 0 ]; then
	sudo $0
	exit
fi

if type mysql > /dev/null 2>&1; then
	printf "MariaDB/MySQL is installed! Good job! :)\n"
else
	printf "How am I supposed to install the database? Install MariaDB pls! :(\n"
	exit 1
fi

if mysql ontime <<< exit 2> /dev/null; then
	printf "On Time database ALREADY INSTALLED!\n  (Please use the update.sh script instead) :(\n"
	exit 1
fi

# Això és perillós perque no en totes les instal·lacions de mariadb l'usuari root
# pot accedir sense password. pero no m'importa lol
sudo mysql << EOF

CREATE DATABASE ontime;
CREATE USER ontime@localhost IDENTIFIED BY 'ontime';
GRANT ALL PRIVILEGES ON ontime.* TO ontime@localhost;
USE ontime;
SOURCE schema.sql;
SOURCE procedures.slq;

EOF

