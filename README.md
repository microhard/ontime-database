# On Time Database

This is the database used by On Time to store accounts, users and markings.

## Installation
To install, simply execute the script `install.sh`, if you already have the database and user, execute the script `update.sh`.

To install the cron task, copy the contents of the file cron/crontab to your crontab and change the path to wherever you have the file ontime_clear.sh on disk, or simply just drop the file ontime_clear.sh inside /etc/cron.daily/

## TODO
We need another sql file with demo data because we waste too much time making accounts and stuff.
