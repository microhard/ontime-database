-- Account:
--   This table stores user accounts
-- -- -- -- -- -- -- -- -- --
-- email:
--   https://datatracker.ietf.org/doc/html/rfc5321#section-4.5.3.1.3
--   https://datatracker.ietf.org/doc/html/rfc2821#section-4.5.3
--   (max 256 characters)
-- password:
--   bcrypt --> hex (always 60 characters)
CREATE TABLE account (
    `id` serial PRIMARY KEY,
    `email` varchar(256) UNIQUE,
    `password` char(60)
);



-- Account Register:
--   This table stores the current state of an account registration.
-- -- -- -- -- -- -- -- -- --
-- status:
--   registered: The user filled the register form
--   confirmed: The user clicked the confirmation email
-- identifier:
--   Random --> SHA256 --> Hex
CREATE TABLE account_register (
    `id` bigint unsigned PRIMARY KEY,
    `status` enum('registered', 'confirmed') DEFAULT 'registered',
    `identifier` char(64) UNIQUE,

    FOREIGN KEY (`id`) REFERENCES account(`id`)
);



-- User:
--   This table stores user sub-accounts, i.e. the ones that mark their time
-- -- -- -- -- -- -- -- -- --
-- card:
--   This is the card number used to mark in physical locations, I don know if 
--   these cards use a standard or have a random number of bytes.
CREATE TABLE user (
    `token` varchar(12) PRIMARY KEY NOT NULL,
    `name` varchar(64),
    `email` varchar(256),
    `card` varchar(16),
    `account_id` bigint unsigned,

    FOREIGN KEY (`account_id`) REFERENCES account(`id`)
);



-- timecard
--   This table stores user clock ins and outs. It is called timecard since
--   in the past workers used actual pshysical cards to clock in with time clocks.
-- -- -- -- -- -- -- -- -- --
CREATE TABLE timecard (
    `id` serial PRIMARY KEY,
    `type` enum('IN', 'OUT'),
    `timestamp` timestamp DEFAULT CURRENT_TIMESTAMP,
    `user_token` varchar(12),

    FOREIGN KEY (`user_token`) REFERENCES user(`token`) ON DELETE CASCADE
);



-- webhook
--   This table stores account webhook configurations. When a user clocks in or
--   out, this table is searched and sends the marking data to the endpoint.
-- -- -- -- -- -- -- -- -- --
-- when:
--   When is the webhook... hooked...
--   clock-in and clock-out or when the user automatically is clocked out by the
--   server cronjob.
-- endpoint:
--   https://datatracker.ietf.org/doc/html/rfc2616
--   The URL to send the request to.
CREATE TABLE webhook (
    `id` serial PRIMARY KEY,
    `when` enum('clock-in', 'clock-out', 'auto-clock-out'),
    `endpoint` varchar(2048),
    `account_id` bigint unsigned,

    FOREIGN KEY (`account_id`) REFERENCES account(`id`)
);
