-- This function creates a new user and creates the token used for the email validation thing
SET GLOBAL log_bin_trust_function_creators = 1;
DROP FUNCTION IF EXISTS register;
DELIMITER $$
CREATE FUNCTION register (email varchar(256), password char(60)) RETURNS char(64)
MODIFIES SQL DATA
BEGIN

    INSERT INTO account (`email`, `password`)
        VALUES (email, password);

    SET @identifier = SHA2(RAND(), 256);
    INSERT INTO account_register (id, identifier)
        VALUES (LAST_INSERT_ID(), @identifier);

    RETURN @identifier;

END$$
DELIMITER ;



-- This procedure marks or unmarks a user automatically
DROP PROCEDURE IF EXISTS mark;
DELIMITER $$
CREATE PROCEDURE mark(token varchar(12))
BEGIN

    SET @last_type = 'OUT';
    SELECT type INTO @last_type FROM timecard WHERE user_token = token ORDER BY id DESC LIMIT 1;

    IF @last_type = 'IN' THEN
        SET @type = 'OUT';
    ELSE
        SET @type = 'IN';
    END IF;

    INSERT INTO timecard (type, user_token) VALUES (@type, token);


END$$
DELIMITER ;


-- This procedure clocks all workers out from office
DROP PROCEDURE IF EXISTS go_home;
DELIMITER $$
CREATE PROCEDURE go_home()
BEGIN

    FOR clocked IN (
        SELECT t.id, t.type, t.user_token
            FROM timecard t
            INNER JOIN (
                    SELECT MAX(id) id
                        FROM timecard
                        GROUP BY user_token
                ) AS l
                ON t.id = l.id
            WHERE t.type = 'IN'
    ) DO
        CALL mark(clocked.user_token);
    END FOR;

END$$
DELIMITER ;
