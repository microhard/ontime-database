#!/bin/bash

# Si no es tenen privilegis root
if [[ $EUID != 0 ]]; then
	sudo $0
	exit
fi

# Fer drop de les taules i un altre cop carregar el schema i els procediments.
sudo mysql ontime << EOF

DROP TABLE IF EXISTS timecard;
DROP TABLE IF EXISTS user;
DROP TABLE IF EXISTS webhook;
DROP TABLE IF EXISTS account_register;
DROP TABLE IF EXISTS account;

SOURCE schema.sql;
SOURCE procedures.sql;

EOF

echo "DONE!"
